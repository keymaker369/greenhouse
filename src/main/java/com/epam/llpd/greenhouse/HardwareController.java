package com.epam.llpd.greenhouse;

import java.io.File;

/**
 *
 * @author Pavel_Vervenko
 */
public interface HardwareController {
    void startFan();
    void stopFan();
    void startWatering();
    void stopWatering();
    void lightOn();
    void lightOff();
    float getTemperature();
    float getHumidity();
    File captureFrame();
    boolean isFanOn();
    public boolean isLightOn();
    public boolean isWateringOn();
}
