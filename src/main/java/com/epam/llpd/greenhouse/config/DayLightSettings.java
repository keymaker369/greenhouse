package com.epam.llpd.greenhouse.config;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Helper Class for DayLight settings.
 * @author Pavel_Vervenko
 */
public class DayLightSettings {

    static SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
    private Date startTime;
    private Date endTime;

    public DayLightSettings(Date startTime, Date endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
        correctTime();
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public static DayLightSettings fromString(String input) {
        String[] split = input.split("-");
        return new DayLightSettings(getDateTime(split[0]), getDateTime(split[1]));
    }

    public static Date getDateTime(String s) {
        Calendar cal = Calendar.getInstance();
        try {
            Date parsed = sdf.parse(s);
            cal.setTime(new Date());
            cal.set(Calendar.HOUR_OF_DAY, parsed.getHours());
            cal.set(Calendar.MINUTE, parsed.getMinutes());
            cal.set(Calendar.SECOND, 0);
            return cal.getTime();
        } catch (ParseException ex) {
            throw new RuntimeException("Wrong time format" + s);
        }
    }

    private void correctTime() {
        Date now = new Date();
        if (startTime.before(now) && endTime.before(now)) {
            System.out.println("Correct time");
            startTime = addToTime(startTime, Calendar.DAY_OF_MONTH, 1);
            endTime = addToTime(endTime, Calendar.DAY_OF_MONTH, 1);
        }
    }

    public static Date addToTime(Date date, int unit, int value) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(unit, 1);
        return cal.getTime();
    }
}
