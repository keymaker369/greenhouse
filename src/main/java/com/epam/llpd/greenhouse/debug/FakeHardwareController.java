package com.epam.llpd.greenhouse.debug;


import com.epam.llpd.greenhouse.HardwareController;
import info.clearthought.layout.TableLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * For debug mode.
 * Hardware Controller - handles logic in case Raspberry Pi doesn't plugged.
 * @author Pavel_Vervenko
 */
public class FakeHardwareController implements HardwareController {
    private final OnOffLabel fanStatus;
    private final OnOffLabel lightStatus;
    private final OnOffLabel waterStatus;
    private final Random random = new Random();
    private final File initialFile = new File("cam/1389551964336.png");
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    
    public FakeHardwareController() {
        JFrame frame = new JFrame("Greenhouse");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(100, 100, 300, 300);

        double size[][] = {{10, 0.5, 0.5}, {40, 40, 40, 40, TableLayout.FILL}};
        TableLayout tableLayout = new TableLayout(size);
        frame.setLayout (tableLayout);
        
        frame.add(new JLabel("Light"), "1, 0");
        frame.add(lightStatus = new OnOffLabel(), "2, 0");
        
        frame.add(new JLabel("Fan"), "1, 1");
        frame.add(fanStatus = new OnOffLabel(), "2, 1");
        
        frame.add(new JLabel("Water"), "1, 2");
        frame.add(waterStatus = new OnOffLabel(), "2, 2");     
    }

    @Override
    public void startFan() {
        fanStatus.on();
    }

    @Override
    public void stopFan() {
        fanStatus.off();
    }

    @Override
    public void startWatering() {
        waterStatus.on();
    }

    @Override
    public void stopWatering() {
        waterStatus.off();
    }

    @Override
    public void lightOn() {
        lightStatus.on();
    }

    @Override
    public void lightOff() {
        lightStatus.off();
    }

    @Override
    public float getTemperature() {
        return random.nextFloat() * (30f - 20f) + 20f;
    }

    @Override
    public float getHumidity() {
        return random.nextFloat() * (70f - 50f) + 50f;
    }

    @Override
    public File captureFrame() {
        File newFile = new File("cam/" + getFileName());
        try {
            BufferedImage img = ImageIO.read(initialFile);
            Graphics2D gr = img.createGraphics();
            gr.setPaint(Color.RED);
            gr.setFont(new Font("Serif", Font.BOLD, 30));
            gr.drawString(sdf.format(new Date()), 50, img.getHeight() - 50);
            gr.dispose();
            ImageIO.write(img, "png", newFile);
            Thread.sleep(2000);
        } catch (Exception ex) {
            Logger.getLogger(FakeHardwareController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return newFile;
    }

    @Override
    public boolean isFanOn() {
        return fanStatus.isOn();
    }

    @Override
    public boolean isLightOn() {
        return lightStatus.isOn();
    }

    @Override
    public boolean isWateringOn() {
        return waterStatus.isOn();
    }
    
    private String getFileName() {
        return System.currentTimeMillis() + ".png";
    }    
}
