package com.epam.llpd.greenhouse;

import com.epam.llpd.greenhouse.debug.FakeHardwareController;
import com.epam.llpd.greenhouse.web.ConfigurationHandler;

/**
 * Application decides whether app is launched on Raspberry or with Emulation Mode
 */
public class App {

    public static Greenhouse createGrowBox(boolean isRunningOnRaspberry) {
        if (isRunningOnRaspberry) {
            return new Greenhouse(ConfigurationHandler.readConfigFromFile(), new PiHardwareController());
        }
        return new Greenhouse(ConfigurationHandler.readConfigFromFile(), new FakeHardwareController());
    }

    public static boolean isRaspberry() {
        return !System.getProperty("os.name").toLowerCase().startsWith("mac");
    }
}
